# 使用Create React App启动

该项目由[Create React App](https://github.com/facebook/create-react-app)创建.

## 可执行命令

在这个项目中，你可以使用以下这些命令：

### `npm start`

用开发模式启动项目
在浏览器中输入[http://localhost:3000](http://localhost:3000)以查看。

当你进行修改的时候，页面将重新刷新。
你也可以在控制台查看输出的错误信息。

### `npm test`

以及时查看的方式运行单元测试用例。
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

将项目打包，打包后的内容将在 `build` 文件夹下。

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
