import "../src/styles/index.scss";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

const styles: React.CSSProperties = {
  textAlign: "center"
}
export const decorators = [
  (Story) => (
    <div style={styles}>
      {Story()}
    </div>
  )
]
