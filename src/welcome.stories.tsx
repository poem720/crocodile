import React from "react";
import { storiesOf } from "@storybook/react";

storiesOf("Welcome page", module)
  .add("welcome", () => {
    return (
      <>
        <h1>欢迎来到 crocodile 组件库</h1>
        <h3>可以使用以下命令安装</h3>
        <code>
          npm install crocodile --save
        </code>
      </>
    )
  }, { info: { disable: true } });
