import React from "react";
import { fireEvent, render, RenderResult, cleanup, waitFor, wait } from "@testing-library/react";
import Menu, { MenuProps } from "./menu";
import MenuItem from "./menuItem";
import SubMenu from "./subMenu";

const testProps: MenuProps = {
  defaultIndex: "0",
  onSelect: jest.fn(),
  className: "test"
}
const testVerticalProps: MenuProps = {
  defaultIndex: "0",
  onSelect: jest.fn(),
  mode: "vertical"
}
const testDefaultSubMenuProps: MenuProps = {
  defaultIndex: "0",
  mode: "vertical",
  onSelect: jest.fn(),
  defaultOpenSubMenus: ["3"]
}
const generateMenu = (props: MenuProps) => {
  return (
    <Menu {...props}>
      <MenuItem>active</MenuItem>
      <MenuItem disabled>disabled</MenuItem>
      <MenuItem>xyz</MenuItem>
      <SubMenu title="dropdown">
        <MenuItem>drop1</MenuItem>
        <MenuItem>drop2</MenuItem>
      </SubMenu>
    </Menu>
  )
}
// 添加一些CSS样式，控制subMenu初始不展示
const createStyleFile = () => {
  const cssFile: string = `
    .submenu {
      display: none;
    }
    .submenu.menu-open {
      display: block;
    }
  `;
  const style = document.createElement("style");
  style.type = "text/css";
  style.innerHTML = cssFile;
  return style;
}

let wrapper: RenderResult, menuElement: HTMLElement, activeElement: HTMLElement, disabledElement: HTMLElement;
describe("test Menu and MenuItem component", () => {
  // 每个case之前都会执行beforeEach这个钩子函数
  beforeEach(() => {
    wrapper = render(generateMenu(testProps));
    wrapper.container.append(createStyleFile());
    menuElement = wrapper.getByTestId("test-menu");
    activeElement = wrapper.getByText("active");
    disabledElement = wrapper.getByText("disabled");
  });
  
  it("should render correct Menu and MenuItem based on default props", () => {
    expect(menuElement).toBeInTheDocument();
    expect(menuElement).toHaveClass("menu test");
    // 这个方法会递归获取到menuElement下面的所有li，因此需要使用CSS伪类去操作
    // expect(menuElement.getElementsByTagName("li").length).toEqual(3);
    expect(menuElement.querySelectorAll(":scope > li").length).toEqual(4);
    expect(activeElement).toHaveClass("menu-item active");
    expect(disabledElement).toHaveClass("menu-item disabled");
  });

  it("click items should change active and call the right callback", () => {
    const thirdItem = wrapper.getByText("xyz");
    fireEvent.click(thirdItem);
    expect(thirdItem).toHaveClass("active");
    expect(activeElement).not.toHaveClass("active");
    expect(testProps.onSelect).toHaveBeenCalledWith("2"); // 方法被调用，且传入参数是2

    fireEvent.click(disabledElement);
    expect(disabledElement).not.toHaveClass("active");
    expect(testProps.onSelect).not.toHaveBeenCalledWith("1");
  });

  it("should render vertical mode when mode is set to vertical", () => {
    cleanup(); // 清空之前的before钩子内容
    const wrapper = render(generateMenu(testVerticalProps));
    const menuElement = wrapper.getByTestId("test-menu");
    expect(menuElement).toHaveClass("menu-vertical");
  });

  it("should show dropdown items when hover on subMenu", async() => {
    expect(wrapper.queryByText("drop1")).not.toBeVisible();
    const dropdownElement = wrapper.getByText("dropdown");
    fireEvent.mouseEnter(dropdownElement);
    // 使用waitFor可以多次执行断言，直到这一项通过/超时
    await waitFor(() => {
      expect(wrapper.queryByText("drop1")).toBeVisible();
    });
    fireEvent.click(wrapper.getByText("drop1"));
    expect(testProps.onSelect).toHaveBeenCalledWith("3-0");
    fireEvent.mouseLeave(dropdownElement);
    await waitFor(() => {
      expect(wrapper.queryByText("drop1")).not.toBeVisible();
    });
  });

  it("click to show dropdown when mode is vertical", () => {
    // 先清空内容，添加vertical模式属性，子菜单暂时不显示
    cleanup();
    const wrapper = render(generateMenu(testVerticalProps));
    wrapper.container.append(createStyleFile());
    expect(wrapper.queryByText("drop1")).not.toBeVisible();
    // 点击子菜单，显示内部内容，点击内部内容触发内部onSelect
    const dropdownElement = wrapper.getByText("dropdown");
    fireEvent.click(dropdownElement);
    expect(wrapper.queryByText("drop1")).toBeVisible();
    fireEvent.click(wrapper.getByText("drop1"));
    expect(testVerticalProps.onSelect).toHaveBeenCalledWith("3-0");
    // 再次点击后，子菜单消失
    fireEvent.click(dropdownElement);
    expect(wrapper.queryByText("drop1")).not.toBeVisible();
  });

  it("show default dropdown items", () => {
    // 先清空内容，在vertical模式下渲染Menu，可以看到设定展开的SubMenu
    cleanup();
    const wrapper = render(generateMenu(testDefaultSubMenuProps));
    wrapper.container.append(createStyleFile());
    expect(wrapper.queryByText("drop1")).toBeVisible();
    fireEvent.click(wrapper.getByText("drop1"));
    expect(testDefaultSubMenuProps.onSelect).toHaveBeenCalledWith("3-0");
    // 再次点击后，子菜单消失
    const dropdownElement = wrapper.getByText("dropdown");
    fireEvent.click(dropdownElement);
    expect(wrapper.queryByText("drop1")).not.toBeVisible();
  });
});
