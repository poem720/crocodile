import React, { useState, createContext } from "react";
import classnames from "classnames";
import { MenuItemProps } from "./menuItem";

type MenuMode = "horizontal" | "vertical"; // 用来替代enum
type SelectCallback = (selectIndex: string) => void;

export interface MenuProps {
    defaultIndex?: string; // 默认高亮部分
    className?: string;
    mode?: MenuMode;
    style?: React.CSSProperties;
    children?: React.ReactNode;
    onSelect?: SelectCallback; // 用户点击选择的回调
    defaultOpenSubMenus?: string[]; // 默认打开的子菜单列表
}
interface IMenuContext {
    index: string;
    onSelect?: SelectCallback;
    mode?: MenuMode;
    defaultOpenSubMenus?: string[]; // 默认打开的子菜单列表
}

export const MenuContext = createContext<IMenuContext>({ index: "0" });

const Menu: React.FC<MenuProps> = (props: MenuProps) => {
    const { className, mode, style, children, defaultIndex, onSelect, defaultOpenSubMenus } = props;
    const [currentActive, setActive] = useState(defaultIndex); // context的初始值
    const classes = classnames("menu", className, {
        [`menu-${mode}`]: mode
    });
    const handleClick = (index: string) => {
        setActive(index);
        onSelect && onSelect(index);
    }
    
    const passedContext: IMenuContext = {
        index: currentActive ? currentActive : "0",
        onSelect: handleClick,
        mode,
        defaultOpenSubMenus
    }
    // 需要对menu的children做出限制
    const renderChildren = () => {
        return React.Children.map(children, (child, index) => {
            // React自带的child属性不定，需要对child进行类型断言
            const childElement = child as React.FunctionComponentElement<MenuItemProps>;
            const { displayName } = childElement.type;
            if (displayName === "MenuItem" || displayName === "SubMenu") {
                // 这里把index属性给添加到每一个子的MenuItem上面，不用再手动一个个添加了
                return React.cloneElement(childElement, { index: index.toString() });
            } else {
                console.error("Warning: Menu should only render children 'MenuItem'");
            }
        });
    }

    return (
        <ul className={classes} style={style} data-testid="test-menu">
            <MenuContext.Provider value={passedContext}>
                {renderChildren()}
            </MenuContext.Provider>
        </ul>
    )
}

Menu.defaultProps = {
    defaultIndex: "0",
    mode: "horizontal",
    defaultOpenSubMenus: []
}

export default Menu;
