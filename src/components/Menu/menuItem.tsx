import React, { useContext } from "react";
import classnames from "classnames";
import { MenuContext } from "./menu";

export interface MenuItemProps {
  index?: string;
  disabled?: boolean;
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode;
}

const MenuItem: React.FC<MenuItemProps> = (props: MenuItemProps) => {
  const { index, disabled, className, style, children } = props;
  const context = useContext(MenuContext);
  const classes = classnames("menu-item", className, {
    "disabled": disabled,
    "active": context.index === index
  }); 
  const handleClick = () => {
    (context.onSelect && !disabled && typeof index === "string") && context.onSelect(index);
  }
  
  return (
    <li className={classes} style={style} onClick={handleClick}>
      {children}
    </li>
  )
}

MenuItem.displayName = "MenuItem"; // React内置静态属性，可以帮助判断类型
export default MenuItem;
