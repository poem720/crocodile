import React, { useState, useEffect } from "react";
import classnames from "classnames";

export type AlertType = "success" | "info" | "danger" | "warning";

export interface AlertProps {
  type?: AlertType;
  message?: String;
  description?: String;
  closable?: boolean;
  delay?: number;
}

const Alert: React.FC<AlertProps> = (props: AlertProps) => {
  const { type, message, description, closable, delay } = props;
  const classes = classnames("alert", {
    [`alert-${type}`]: type
  });
  const [show, setShow] = useState(true);

//   TODO: 对于延时关闭的效果测试暂不考虑
//   useEffect(() => {
//       !closable && setTimeout(() => {
//           setShow(false);
//       }, delay)
//   });

  return (
    show ?
    (
        <div className={classes}>
            {closable ? <span className="alert-close" onClick={() => {setShow(false)}}>关闭</span> : null}
            <div className="alert-message">{message}</div>
            <div className="alert-description">{description}</div>
        </div>
    ): null
  );
};

Alert.defaultProps = {
  type: "info",
  message: "Message",
  description: "Description",
  closable: false,
  delay: 3000
};

export default Alert;
