import React, { Fragment } from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Alert from "./alert";

const defaultAlert = () => (
  <Fragment>
    <Alert />
  </Fragment>
);

const alertWithType = () => (
  <Fragment>
    <Alert type="info" message="提示Alert" description="提示文字" />
    <Alert type="success" message="成功Alert" description="成功文字" />
    <Alert type="danger" message="失败Alert" description="失败文字" />
    <Alert type="warning" message="警告Alert" description="警告文字" />
  </Fragment>
);

const closableAlert = () => (
  <Alert closable />
);

storiesOf("警告信息 Alert", module)
  .add("默认Alert", defaultAlert)
  .add("不同样式的Alert", alertWithType)
  .add("可关闭的Alert", closableAlert);