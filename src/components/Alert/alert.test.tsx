import React from "react";
import { fireEvent, render } from "@testing-library/react";
import Alert, { AlertProps } from "./alert";

const defaultProps: AlertProps = {
    message: "test"
}
const closableProps: AlertProps = {
    type: "success",
    closable: true
}

describe("test Alert component", () => {
    // 正确渲染默认的alert
    it("render correct default alert", () => {
        // 先检测是否添加正确（元素在HTML里面，有两个节点，节点里面都是默认的值）
        const { container } = render(<Alert {...defaultProps} />);
        expect(container).toBeInTheDocument();
        expect(container.querySelector(".alert")).toHaveClass("alert-info");
        // 标题节点
        const message = container.querySelector(".alert-message");
        expect(message).toBeTruthy();
        expect(message?.innerHTML).toEqual("test");
        // 内容节点
        const description = container.querySelector(".alert-description");
        expect(description).toBeTruthy();
        expect(description?.innerHTML).toEqual("Description");
        // 没有关闭按钮
        const closeIcon = container.querySelector(".alert-close");
        expect(closeIcon).toBeFalsy();
    });

    // 检测点击关闭事件
    it("close after click", () => {
        const { container } = render(<Alert {...closableProps} />);
        expect(container).toBeInTheDocument();
        expect(container.querySelector(".alert")).toHaveClass("alert-success");
        // 检测是否有关闭按钮
        const closeArea = container.querySelector(".alert-close");
        expect(closeArea).toBeTruthy();
        expect(closeArea?.innerHTML).toEqual("关闭");
        // 检测关闭按钮点击后是否可获得元素
        fireEvent.click(closeArea as Element);
        expect(container.querySelector(".alert")).toBeFalsy();
    });
});
