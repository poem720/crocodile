import React, { ReactElement, InputHTMLAttributes } from "react";
import classnames from "classnames";
import { IconProp } from "@fortawesome/fontawesome-svg-core";

type InputSize = "lg" | "sm";
export interface InputProps extends Omit<InputHTMLAttributes<HTMLElement>, "size"> {
  disabled?: boolean;
  size?: InputSize;
  prepand?: string | ReactElement;
  append?: string | ReactElement;
  icon?: IconProp;
}

const Input: React.FC<InputProps> = (props: InputProps) => {
  // 先取出各种属性
  const { disabled, size, prepand, append, icon } = props;
  // 再根据各种属性生成不同的class
  const classes = classnames("input", {
    [`input-${size}`]: size,
    "disabled": disabled
  });
  // 最后渲染不同的节点
  return (
    <>
      {
        prepand ? <></> : null
      }
      <input className={classes} />
      {
        append ? <></> : null
      }
    </>
  )
}

Input.defaultProps = {}

export default Input;
