import React from "react";
import classnames from "classnames";

export type ButtonSize = "lg" | "sm";

export type ButtonType = "primary" | "default" | "danger" | "link";

interface BaseButtonProps {
    className?: string;
    disabled?: boolean;
    size?: ButtonSize;
    btnType?: ButtonType;
    children: React.ReactNode;
    href?: string;
}
// 所有button的属性
type NativeButtonProps = BaseButtonProps & React.ButtonHTMLAttributes<HTMLElement>;
type AnchorButtonProps = BaseButtonProps & React.AnchorHTMLAttributes<HTMLElement>;
export type ButtonProps = Partial<NativeButtonProps & AnchorButtonProps>;

const Button:React.FC<ButtonProps> = (props) => {
    const { btnType, disabled, size, children, href, className, ...restProps } = props;
    // 默认class是btn，另外根据属性添加一些特有的class
    // 要可以添加用户自添加的className
    const classes = classnames("btn", className, {
        [`btn-${btnType}`]: btnType,
        [`btn-${size}`]: size,
        "disabled": (btnType === "link") && disabled
    });
    // 只有同时指定类型并给出href，才展示a标签
    if (btnType === "link" && href) {
        return (
            <a
                className={classes}
                href={href}
                {...restProps}
            >
                {children}
            </a>
        )
    } else {
        return (
            <button
                className={classes}
                disabled={disabled}
                {...restProps}
            >
                {children}
            </button>
        )
    }
}

// 默认属性
Button.defaultProps = {
    disabled: false,
    btnType: "default"
}

export default Button;
