import React from "react";
import { fireEvent, render } from "@testing-library/react";
import Button, { ButtonProps } from "./button";

const defaultProps = {
    onClick: jest.fn()
}
const primaryLargeProps: ButtonProps = {
    btnType: "primary",
    size: "lg",
    className: "test-class"
}
const anchorProps: ButtonProps = {
    btnType: "link",
    href: "www.baidu.com"
}
const disabledProps: ButtonProps = {
    disabled: true,
    onClick: jest.fn()
}

describe("test Button component", () => {
    it("render correct default button", () => {
        const wrapper = render(<Button {...defaultProps}>default</Button>);
        // getByText返回的一定是HTMLElement，而query的话返回的可能是null
        const element = wrapper.getByText("default") as HTMLButtonElement;
        // 测试button是否添加正确，标签名称、class是否正确
        expect(element).toBeInTheDocument();
        expect(element.tagName).toEqual("BUTTON");
        expect(element).toHaveClass("btn btn-default");
        expect(element.disabled).toBeFalsy();
        // 测试用户交互行为
        fireEvent.click(element);
        expect(defaultProps.onClick).toHaveBeenCalled();
    });

    it("render correct component based on different props", () => {
        const wrapper = render(<Button {...primaryLargeProps}>primary large</Button>);
        const element = wrapper.getByText("primary large");
        expect(element).toBeInTheDocument();
        // 这里主要检测class是否正确
        expect(element).toHaveClass("btn btn-primary btn-lg test-class");
    });

    it("render a link when type and href is correct", () => {
        const wrapper = render(<Button {...anchorProps}>Link</Button>);
        const element = wrapper.getByText("Link");
        expect(element).toBeInTheDocument();
        expect(element.tagName).toEqual("A");
        expect(element).toHaveClass("btn btn-link");
    });

    it("render disabled button", () => {
        const wrapper = render(<Button {...disabledProps}>disabled</Button>);
        const element = wrapper.getByText("disabled") as HTMLButtonElement;
        expect(element).toBeInTheDocument();
        expect(element.tagName).toEqual("BUTTON");
        expect(element).toHaveClass("btn btn-default");
        // disabled属性值为true
        expect(element.disabled).toBeTruthy();
        // 检测click是否不能点击
        fireEvent.click(element);
        expect(disabledProps.onClick).not.toHaveBeenCalled();
    });
});
