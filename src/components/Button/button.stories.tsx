import React, { Fragment } from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Button from "./button";

const defaultButton = () => (
  <Button onClick={action("clicked")}>default button</Button>
);

const buttonWithSize = () => (
  <Fragment>
    <Button size="lg">large</Button>
    <Button size="sm">small</Button>
  </Fragment>
);

const buttonWithType = () => (
  <Fragment>
    <Button btnType="primary">primary button</Button>
    <Button btnType="danger">danger button</Button>
    <Button btnType="link" href="https://www.baidu.com">link button</Button>
  </Fragment>
);

storiesOf("按钮 Button", module)
  .add("默认Button", defaultButton)
  .add("不同尺寸的Button", buttonWithSize)
  .add("不同样式的Button", buttonWithType);
