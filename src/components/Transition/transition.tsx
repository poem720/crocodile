import React from "react";
import { CSSTransition } from "react-transition-group";
import { CSSTransitionProps } from "react-transition-group/CSSTransition";

type AnimationName = "zoom-in-top" | "zoom-in-right" | "zoom-in-bottom" | "zoom-in-left";

type TransitionProps = CSSTransitionProps & {
  animation?: AnimationName;
  wrapper?: boolean; // 这里是为了包裹子元素，让父子元素的transition属性不互相影响
}

const Transition: React.FC<TransitionProps> = (props: TransitionProps) => {
  const { children, classNames, animation, wrapper, ...restProps } = props;
  
  return (
    <CSSTransition
      classNames={classNames ? classNames : animation}
      {...restProps}
    >
      {wrapper ? <div>{children}</div> : children}
    </CSSTransition>
  )
}
Transition.defaultProps = {
  unmountOnExit: true,
  appear: true
}

export default Transition;
