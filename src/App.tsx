import React, { useState } from 'react';
import Transition from "./components/Transition/transition";
import Button from "./components/Button/button";
import Menu from "./components/Menu/menu";
import SubMenu from "./components/Menu/subMenu";
import MenuItem from "./components/Menu/menuItem";
// import Icon from "./components/Icon/icon";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);

const App:React.FC = () => {
  const [show, setShow] = useState(false);
  
  return (
    <div className="App">
      <Button onClick={() => {setShow(!show)}}>click to toggle</Button>
      <Transition
        in={show}
        timeout={300}
        animation="zoom-in-left"
      >
        <div>
          <p>hello 1</p>
          <p>hello 2</p>
          <p>hello 3</p>
        </div>
      </Transition>
      <Transition
        in={show}
        timeout={300}
        animation="zoom-in-top"
        wrapper
      >
        <Button>test</Button>
      </Transition>
      {/* <Icon icon="coffee" theme="primary" size="10x" /> */}
      <Menu defaultIndex="0" onSelect={(index) => {console.log(index)}}>
        <MenuItem>item1</MenuItem>
        <SubMenu title="item2">
          <MenuItem>item2-1</MenuItem>
          <MenuItem>item2-2</MenuItem>
          <MenuItem>item2-3</MenuItem>
        </SubMenu>
        <MenuItem>item3</MenuItem>
      </Menu>
      <Menu mode="vertical" defaultOpenSubMenus={["1"]} defaultIndex="0" onSelect={(index) => {console.log(index)}}>
        <MenuItem>item1</MenuItem>
        <SubMenu title="item2">
          <MenuItem>item2-1</MenuItem>
          <MenuItem>item2-2</MenuItem>
          <MenuItem>item2-3</MenuItem>
        </SubMenu>
        <MenuItem>item3</MenuItem>
      </Menu>
    </div>
  );
}

export default App;
